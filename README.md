## About Me

Tim is passionate about identifying ways to shift IT culture to be more focused on automation, metrics-gathering, and sharing measurements and processes in the hopes to accelerate the rate at which business partners realize value from the solutions developed.

Tim is a senior-level software engineer, currently serving as manager of a pre-sales engineering team that spans 4 continents. His career has spanned across the Infrastructure, System Administration, Software Development, and Data Analytics worlds.

Most recently, Tim is operating as a pre-sales leader of people in the commercial segment of GitLab's business and helping customers and prospects articulate their business initiatives and current pain points while aligning the GitLab product offering to help achieve those initiatives and remediate the source of their pains.
